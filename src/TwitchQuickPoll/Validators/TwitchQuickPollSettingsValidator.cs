﻿using FluentValidation;
using TwitchQuickPoll.Models;

namespace TwitchQuickPoll.Validators
{
    public class TwitchQuickPollSettingsValidator : AbstractValidator<TwitchQuickPollSettings>
    {
        public TwitchQuickPollSettingsValidator()
        {
            RuleFor(settings => settings.ClientId).NotNull().NotEmpty();
            RuleFor(settings => settings.AccessToken).NotNull().NotEmpty();
            RuleFor(settings => settings.BroadcasterID).NotNull();
        }
    }
}
