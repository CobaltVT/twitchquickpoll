﻿using FluentValidation;
using TwitchQuickPoll.Models;

namespace TwitchQuickPoll.Validators
{
    public sealed class TwitchQuickPollCreateOptionValidator : AbstractValidator<TwitchQuickPollCreateOption>
    {
        public TwitchQuickPollCreateOptionValidator() 
        {
            RuleFor(create => create.Title).NotNull().MinimumLength(1).MaximumLength(60);
            RuleFor(create => create.Title).Must(title => String.IsNullOrWhiteSpace(title) == false).WithMessage("Titles cannot be empty.")
                .When(create => create.Title.Count() > 0);

            RuleFor(create => create.Questions).NotNull().Must(q => q.Count() >= 2 && q.Count() <= 5).WithMessage("A poll requires a minimum of two and a maximum of five questions.").ForEach(q => 
            {
                q.Must(question => String.IsNullOrWhiteSpace(question) == false).WithMessage("Questions cannot be empty.")
                .Must(question => question.Count() <= 25).WithMessage("Questions have a maximum of 25 characters.");
            });

            RuleFor(create => create.Duration).InclusiveBetween(60, 600);
            RuleFor(create => create.ChannelPoints).NotNull().GreaterThan(0).When(create => create.UseChannelPoints == true);
        }
    }
}
