﻿namespace TwitchQuickPoll.Models
{
    public class TwitchQuickPollSettings
    {
        public string ClientId { get; set; }
        public string AccessToken { get; set; }
        public string BroadcasterID { get; set; }
    }
}
