﻿using CommandLine;

namespace TwitchQuickPoll.Models
{
    /// <summary>
    /// The options class that we use to control input from the console
    /// </summary>
    [Verb("create", aliases: ["c"], HelpText = "Create a poll on Twitch.")]
    public class TwitchQuickPollCreateOption
    {
        /// <summary>
        /// The title of the poll to be created..
        /// </summary>
        [Option('t', "title", Required = true, HelpText = "The title of your poll (minimum value 1, maximum value 60).")]
        public required string Title { get; set; }

        /// <summary>
        /// A list of questions for the created poll.
        /// </summary>
        [Option('q', "question", Required = true, Min = 2, Max = 5, HelpText = "A question for the poll (minimum value 2, maximimum value 60).")]
        public IEnumerable<string> Questions { get; set; }

        /// <summary>
        /// The duration that the poll should run for.
        /// </summary>
        [Option('d', "duration", Required = false, Default = 60, HelpText = "The duration that the poll should run for (minimum value 60, maximum value 600)")]
        public int Duration { get; set; }

        /// <summary>
        /// If channel points can be redeemed for additional votes.
        /// </summary>
        [Option('u', "usechannelpoints", Required = false, Default = false, HelpText = "Can channel points be redeemed for additional votes in the poll.")]
        public bool UseChannelPoints { get; set; }

        /// <summary>
        /// The cost of additional votes if points can be redeemed for additional votes.
        /// </summary>
        [Option('p', "points", Required = false, Default = null, HelpText = "How many channel points does an additional vote cost.")]
        public int? ChannelPoints { get; set; }
    }
}
