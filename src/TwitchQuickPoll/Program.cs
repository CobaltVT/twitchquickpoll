﻿using CommandLine;
using FluentValidation.Results;
using Microsoft.Extensions.Configuration;
using ScottPlot;
using System.Text.Json;
using TwitchLib.Api;
using TwitchLib.Api.Core.Exceptions;
using TwitchLib.Api.Helix.Models.Polls.CreatePoll;
using TwitchLib.Api.Helix.Models.Polls.GetPolls;
using TwitchLib.Api.Helix.Models.Users.GetUsers;
using TwitchQuickPoll.Extensions;
using TwitchQuickPoll.Models;
using TwitchQuickPoll.Validators;

namespace TwitchQuickPoll
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            ParserResult<TwitchQuickPollCreateOption> parseResult = Parser.Default.ParseArguments<TwitchQuickPollCreateOption>(args);
            if (parseResult.Errors.Any())
            {
                return; //Bail out     
            }

            TwitchQuickPollCreateOptionValidator validator = new TwitchQuickPollCreateOptionValidator();

            ValidationResult validationResult = await validator.ValidateAsync(parseResult.Value);

            if (validationResult.IsValid == false)
            {
                validationResult.WriteValidationMessagesToOutput();
                return;
            }

            //TODO: Clean this up later build it better
            var builder = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            TwitchQuickPollSettings? appSettings = config.GetSection("TwitchQuickPollSettings").Get<TwitchQuickPollSettings>();

            if (appSettings is null)
            {
                throw new Exception("Unable to retrieve application settings information from appsettings.json.");
            }

            TwitchQuickPollSettingsValidator settingsValidator = new TwitchQuickPollSettingsValidator();
            ValidationResult settingsValidationResult = await settingsValidator.ValidateAsync(appSettings!);

            if (settingsValidationResult.IsValid == false)
            {
                settingsValidationResult.WriteValidationMessagesToOutput();
                return;
            }

            TwitchAPI api = new TwitchAPI();
            api.Settings.ClientId = appSettings.ClientId;
            api.Settings.AccessToken = appSettings.AccessToken;

            try
            {
                CreatePollRequest poll = new CreatePollRequest();

                string broadcasterID = await HandleBroadcasterID(api, appSettings);

                poll.Title = parseResult.Value.Title;
                poll.DurationSeconds = parseResult.Value.Duration;

                List<Choice> choices = new List<Choice>();
                foreach (string question in parseResult.Value.Questions)
                {
                    Choice choice = new Choice();
                    choice.Title = question;
                    choices.Add(choice);
                }

                poll.Choices = choices.ToArray();

                poll.ChannelPointsVotingEnabled = parseResult.Value.UseChannelPoints;
                poll.ChannelPointsPerVote = parseResult.Value.ChannelPoints ?? 0;
                poll.BroadcasterId = broadcasterID;

                //Note: If a poll is already running this will throw a confusing error message. 
                CreatePollResponse createPollResponse = await api.Helix.Polls.CreatePollAsync(poll);

                Thread.Sleep((poll.DurationSeconds * 1000) + 1000);

                var pollResponse = await api.Helix.Polls.GetPollsAsync(broadcasterID, new List<string>() { createPollResponse.Data.FirstOrDefault().Id });

                ProcessPoll(pollResponse.Data.FirstOrDefault());
            }
            catch (ClientIdAndOAuthTokenRequired)
            {
                //TODO: Log me
                Console.WriteLine("You need to specify both the client ID and oauth token to create a poll. Please check appsettings.json");
            }
            catch (Exception ex) //Note: By default TwitchLib returns an error message blaming bad client ID/access token for it's problems.
            {
                //TODO: Log me
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Handles the broadcaster ID so that we can use it going forward.
        /// 
        /// If a broadcaster ID is passed in (i.e. if it was provided from the command line) then we return it without processing. 
        /// If the app settings file contains a broadcaster ID then we return it without processing.
        /// If no broadcaster ID is known then we query the user and retrieve their broadcaster ID. In the interest of minimizing 
        /// calls to the Twitch API we then write that broadcaster ID to our appsettings file to be used going forward.
        /// </summary>
        /// <param name="api">The instance of the Twitch API to interact with Twitch.</param>
        /// <param name="appSettings">The application settings containing our keys for interacting with Twitch</param>
        /// <returns>
        /// The broadcaster ID. 
        /// </returns>
        /// <exception cref="Exception">Throwing </exception>
        private static async Task<string> HandleBroadcasterID(TwitchAPI api, TwitchQuickPollSettings appSettings)
        {
            string broadcasterID = "";
            if (string.IsNullOrWhiteSpace(appSettings.BroadcasterID))
            {
                GetUsersResponse userResponse = await api.Helix.Users.GetUsersAsync();

                if (userResponse is null || userResponse.Users is null || userResponse.Users.Any() == false)
                {
                    throw new Exception("Unable to retrieve necessary user information to populate broadcaster ID.");
                }

                var user = userResponse!.Users!.First();

                if (user.BroadcasterType == "")
                {
                    throw new Exception("The user in question does not have the ability to create polls. Polls can only be created by Twitch affiiate or partner accounts.");
                }

                broadcasterID = user.Id;

                if (broadcasterID is null)
                {
                    throw new Exception("Unable to retrieve broadcaster ID from user.");
                }

                appSettings.BroadcasterID = broadcasterID!;

                var options = new JsonSerializerOptions { WriteIndented = true };
                string json = string.Format("{{ \"TwitchQuickPollSettings\": {0} }}", JsonSerializer.Serialize(appSettings, options));

                string appsettingsPath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");

                File.WriteAllText(appsettingsPath, json);
                return broadcasterID;
            }
            else
            {
                broadcasterID = appSettings.BroadcasterID;
            }

            return broadcasterID;
        }

        private static void ProcessPoll(TwitchLib.Api.Helix.Models.Polls.Poll poll) //TODO: Specify return type (image [default] VS csv)
        {
            int regularVoteColour = 2;
            int channelPointVoteColour = 1;

            Plot pollPlot = new Plot();
            ScottPlot.Palettes.OneHalfDark palette = new();
            List<Bar> bars = new List<Bar>();
            List<Tick> ticks = new List<Tick>();

            for (int i = 0; i < poll.Choices.Count(); i++)
            {
                var choice = poll.Choices[i];
                ticks.Add(new Tick(i, choice.Title));

                int votes = choice.Votes - choice.ChannelPointsVotes;

                bars.Add(new Bar() { Position = i, ValueBase = 0, Value = votes, FillColor = palette.GetColor(regularVoteColour) }); //Regular votes
                bars.Add(new Bar() { Position = i, ValueBase = votes, Value = choice.Votes, FillColor = palette.GetColor(channelPointVoteColour) }); //Channel point votes
            }

            pollPlot.Add.Bars(bars);

            pollPlot.Axes.Bottom.TickGenerator = new ScottPlot.TickGenerators.NumericManual(ticks.ToArray());
            pollPlot.Axes.Bottom.MajorTickStyle.Length = 0;
            pollPlot.Axes.Left.TickGenerator = new ScottPlot.TickGenerators.NumericAutomatic();
            pollPlot.HideGrid();

            pollPlot.Style.ColorAxes(Color.FromHex("#d7d7d7"));
            pollPlot.Style.ColorGrids(Color.FromHex("#404040"));
            pollPlot.Style.Background(
                figure: Color.FromHex("#181818"),
                data: Color.FromHex("#1f1f1f"));
            pollPlot.Style.ColorLegend(
                background: Color.FromHex("#404040"),
                foreground: Color.FromHex("#d7d7d7"),
                border: Color.FromHex("#d7d7d7"));

            // tell the plot to autoscale with no padding beneath the bars
            pollPlot.Axes.Margins(bottom: 0);

            pollPlot.XLabel(poll.Title);
            pollPlot.YLabel("Viewer Votes");


            pollPlot.Legend.IsVisible = true;
            pollPlot.Legend.Location = Alignment.UpperRight;
            pollPlot.Legend.ManualItems.Add(new() { Label = "Regular Votes", FillColor = palette.GetColor(regularVoteColour) });
            pollPlot.Legend.ManualItems.Add(new() { Label = "Channel Point Votes", FillColor = palette.GetColor(channelPointVoteColour) });

            DateTime pollTime = DateTime.Now;
            //We need to sanitize the title for the filepath
            string graphFilePath = Path.Combine(Directory.GetCurrentDirectory(), $"{pollTime.ToLongDateString()}{pollTime.ToLongTimeString()} {poll.Title}.png".SanitizeFileName());
            pollPlot.SavePng(graphFilePath, 1000, 1000); //TODO: Pass me in?
        }

        //TODO: Retrieve list of all availiable historical polls

        //TODO: Retrieve image specified poll
    }
}