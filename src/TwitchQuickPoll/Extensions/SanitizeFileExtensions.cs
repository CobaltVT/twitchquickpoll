﻿namespace TwitchQuickPoll.Extensions
{
    public static class SanitizeFileExtensions
    {
        public static string SanitizeFileName(this string filename)
        {
            string result = filename;

            foreach (char invalidCharacter in Path.GetInvalidFileNameChars())
            {
                result = result.Replace(invalidCharacter.ToString(), String.Empty);
            }

            return result;
        }
    }
}
