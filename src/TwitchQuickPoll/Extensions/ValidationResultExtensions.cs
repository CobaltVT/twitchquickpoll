﻿using FluentValidation.Results;

namespace TwitchQuickPoll.Extensions
{
    public static class ValidationResultExtensions
    {
        public static void WriteValidationMessagesToOutput(this ValidationResult validationResult)
        {
            if (validationResult is null)
            {
                return;
            }

            if (validationResult.IsValid == false)
            {
                foreach (var error in validationResult.Errors)
                {
                    Console.WriteLine(error);
                }
            }

            return;
        }
    }
}
